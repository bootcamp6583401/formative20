package formative.twenty.formative20;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import formative.twenty.formative20.models.Comment;
import formative.twenty.formative20.repositories.CommentRepository;
import formative.twenty.formative20.services.impl.CommentServiceImpl;


@SpringBootTest
public class CommentServiceTest {
    @Mock
    private CommentRepository repo;

    @InjectMocks
    private CommentServiceImpl service;

    @Test
    public void whenGetAllComments_thenReturnCommentList() {
        Comment obj = new Comment();
        obj.setId(1);
        obj.setName("Test Comment");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Comment> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test Comment", result.get(0).getName());
    }

    @Test
    public void whenGetCommentById_thenReturnComment() {
        Comment obj = new Comment();
        obj.setId(1);
        obj.setName("Test Comment");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Comment result = service.getById(1);

        assertEquals("Test Comment", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));

        Comment result = service.getById(1);

        assertNull(result);
    }

    @Test
    public void whenSaveComment_thenCommentShouldBeSaved() {
        Comment obj = new Comment();
        obj.setId(1);
        obj.setName("Test Comment");

        when(repo.save(any(Comment.class))).thenReturn(obj);

        Comment savedComment = service.save(obj);

        assertNotNull(savedComment);
        assertEquals(obj.getName(), savedComment.getName());
        verify(repo, times(1)).save(obj);
    }


    @Test
    public void whenDeleteComment_thenCommentShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateComment_thenCommentShouldBeUpdated() {
        Comment existingComment = new Comment();
        existingComment.setId(1);
        existingComment.setName("Old Comment Name");

        Comment updated = new Comment();
        updated.setId(1);
        updated.setName("Updated Comment Name");

        when(repo.findById(existingComment.getId())).thenReturn(Optional.of(existingComment));
        when(repo.save(any(Comment.class))).thenAnswer(i -> i.getArguments()[0]);

        Comment updatedComment = service.updateById(existingComment.getId(), updated);

        assertNotNull(updatedComment);
        assertEquals(updated.getName(), updatedComment.getName());
        verify(repo, times(1)).save(updatedComment);
    }

}
