package formative.twenty.formative20;



import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.Test;

import formative.twenty.formative20.models.Comment;


public class CommentTest {
    @Test
    void testCommentGettersAndSetters() {
        Comment obj = new Comment();

        obj.setId(1);
        obj.setBody("Body");
        obj.setEmail("an@email.com");
        obj.setName("Test Comment");
        obj.setPostId(1);
        obj.setName("Swag");


        assertEquals(1, obj.getId());
        assertEquals("Swag", obj.getName());
        assertEquals("an@email.com", obj.getEmail());
        assertEquals(1, obj.getPostId());
        assertEquals("Body", obj.getBody());
    }
}
