package formative.twenty.formative20;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import formative.twenty.formative20.controllers.CommentController;
import formative.twenty.formative20.models.Comment;
import formative.twenty.formative20.services.CommentService;
import formative.twenty.formative20.services.impl.CommentServiceImpl;

import java.util.List;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CommentController.class)
public class CommentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentServiceImpl service;

    @Test
    public void getAllComments_thenReturnJsonArray() throws Exception {
        Comment comment1 = new Comment();
        comment1.setId(1);
        comment1.setName("Comment1");

        Comment comment2 = new Comment();
        comment2.setId(2);
        comment2.setName("Comment2");

        List<Comment> allComments = Arrays.asList(comment1, comment2);

        given(service.getAll()).willReturn(allComments);

        mockMvc.perform(get("/comments")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(comment1.getName())))
                .andExpect(jsonPath("$[1].name", is(comment2.getName())));
    }

    @Test
    public void getCommentById_WhenCommentExists() throws Exception {
        int commentId = 1;
        Comment comment = new Comment();
        comment.setId(commentId);
        comment.setName("Comment1");

        given(service.getById(commentId)).willReturn(comment);

        mockMvc.perform(get("/comments/{id}", commentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(comment.getName())));
    }

    @Test
    public void createComment_WhenPostComment() throws Exception {
        Comment comment = new Comment();
        comment.setId(1);
        comment.setName("Comment1");

        given(service.save(any(Comment.class))).willReturn(comment);

        mockMvc.perform(post("/comments")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Comment1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(comment.getName())));
    }

    @Test
    public void deleteComment_WhenCommentExists() throws Exception {
        int commentId = 1;

        doNothing().when(service).deleteById(commentId);

        mockMvc.perform(delete("/comments/{id}", commentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateComment_WhenPutComment() throws Exception {
        int commentId = 1;
        Comment comment = new Comment();
        comment.setId(commentId);
        comment.setName("Updated Comment");

        given(service.updateById(eq(commentId), any(Comment.class))).willReturn(comment);

        mockMvc.perform(put("/comments/{id}", commentId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated Comment\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(comment.getName())));
    }
}