package formative.twenty.formative20.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import formative.twenty.formative20.models.Comment;
import formative.twenty.formative20.services.impl.CommentServiceImpl;


@RestController
@RequestMapping("/comments")
public class CommentController {

    @Autowired
    CommentServiceImpl service;

    @GetMapping
    public Iterable<Comment> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Comment save(@RequestBody Comment product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Comment getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Comment updateById(@PathVariable int id, @RequestBody Comment product) {
        return service.updateById(id, product);
    }

}