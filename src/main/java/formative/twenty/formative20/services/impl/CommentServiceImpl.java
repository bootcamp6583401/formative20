package formative.twenty.formative20.services.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.twenty.formative20.models.Comment;
import formative.twenty.formative20.repositories.CommentRepository;
import formative.twenty.formative20.services.CommentService;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentRepository repo;

    public List<Comment> getAll() {
        return repo.findAll();
    }

    public Comment save(Comment comment) {
        return repo.save(comment);
    }

    public Comment getById(int id) {
        Optional<Comment> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Comment updateById(int id, Comment comment) {

        comment.setId(id);
        return repo.save(comment);

    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}