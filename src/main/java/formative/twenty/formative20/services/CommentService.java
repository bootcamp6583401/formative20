package formative.twenty.formative20.services;


import java.util.List;

import formative.twenty.formative20.models.Comment;


public interface CommentService {
    List<Comment> getAll();

    Comment getById(int id);

    Comment save(Comment obj);

    void deleteById(int id);

    Comment updateById(int id, Comment brand);

}
