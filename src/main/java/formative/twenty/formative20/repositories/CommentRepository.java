package formative.twenty.formative20.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.twenty.formative20.models.Comment;


public interface CommentRepository extends CrudRepository<Comment, Integer> {

    @Query(value ="SELECT * FROM comment", nativeQuery = true)
    List<Comment> findAll();
}
