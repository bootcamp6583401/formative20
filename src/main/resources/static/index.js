const getBtn = document.querySelector("#get-btn");

const getNewComments = async () => {
  const res = await fetch(
    "https://jsonplaceholder.typicode.com/posts/1/comments"
  );

  const comments = await res.json();

  return comments;
};

getBtn.addEventListener("click", async () => {
  const comments = await getNewComments();
  await fetch("http://localhost:8080/comments/many", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(comments.map((c) => ({ ...c, id: undefined }))),
  });

  displayComments();
});

const list = document.querySelector("#comments-list");

const getSavedComments = async () => {
  const res = await fetch("http://localhost:8080/comments");

  const comments = await res.json();

  return comments;
};

const displayComments = async () => {
  list.innerHTML = "";
  const comments = await getSavedComments();

  comments.forEach((c) => {
    const commentRow = document.createElement("li");
    commentRow.className = "px-4 py-2 border border-zinc-400";
    commentRow.innerHTML = `
  <div class="text-sm font-bold">${c.name}</div>
  <div>${c.body}</div>
  `;
    list.appendChild(commentRow);
  });
};

displayComments();
