global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve([{ id: 1, name: "name", email: "email", body: "body" }]),
  })
);

beforeEach(() => {
  fetch.mockClear();
});

test("comine", async () => {
  document.body.innerHTML = `<main class="">
  <section class="flex flex-col items-center justify-center mb-8">
    <h1 class="text-4xl font-bold mb-4">Get Data from JSON Placeholder</h1>
    <button
      id="get-btn"
      class="bg-blue-900 text-white font-semibold px-4 py-2 rounded-md text-2xl"
    >
      Get Data
    </button>
  </section>
  <section class="flex flex-col items-center justify-center">
    <h2 class="font-bold text-2xl mb-4">Saved Comments</h2>
    <ul id="comments-list" class="flex flex-col gap-2 w-96 max-w-full"></ul>
  </section>
</main>`;

  // fetch = async () => {
  //   return {
  //     json: async function () {
  //       return [{ id: 1, name: "name", email: "email", body: "body" }];
  //     },
  //   };
  // };

  const getBtn = document.querySelector("#get-btn");
  const list = document.querySelector("#comments-list");
  await getBtn.click();
  console.log(Array.from(list.querySelectorAll("li")).length);

  require("./index");
  expect(global.fetch).toHaveBeenCalledTimes(1);
});
